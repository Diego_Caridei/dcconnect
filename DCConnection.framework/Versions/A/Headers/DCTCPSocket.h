//
//  DCTCPSocket.h
//  socket
//
//  Created by Flexkid on 15/07/13.
//  Copyright (c) 2013 Diego Caridei. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <CFNetwork/CFNetwork.h>
@interface DCTCPSocket : NSObject<NSStreamDelegate>

- (void)open;
- (void)close;
//-(void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent;
-(void)sendMessage:(NSString*)data;
- (void)connectionforIpAddress:(NSString *)ipAddress forPort:(NSString *)portNo;
-(NSString*)Reading;
-(DCTCPSocket*)initWithAddressAndPort:(NSString*)address :(NSString*)port;


@end
