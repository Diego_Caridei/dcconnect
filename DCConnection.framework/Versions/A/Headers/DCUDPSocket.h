//
//  DCUDPSocket.h
//  DCTSocketExample1
//
//  Created by Flexkid on 16/07/13.
//  Copyright (c) 2013 Diego Caridei. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
@interface DCUDPSocket : NSObject
-(void)Connection:(NSString*)hostname :(NSString*)PortNumber;
-(void)sendMessage:(NSString*)Message;
-(NSMutableString*)Read;
-(void)Reading;
-(DCUDPSocket*)initWithAddressAndPort:(NSString*)address :(NSString*)port;

@property(nonatomic) NSMutableString *contenuto, *messaggioRicevuto;
@property(nonatomic) int sd,ret;
@end
