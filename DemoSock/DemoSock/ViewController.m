//
//  ViewController.m
//  DemoSock
//
//  Created by Flexkid on 21/10/13.
//  Copyright (c) 2013 Diego Caridei. All rights reserved.
//

#import "ViewController.h"
@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *message;
- (IBAction)write:(id)sender;

- (IBAction)read:(id)sender;
@end

@implementation ViewController
DCTCPSocket *tcp;
DCUDPSocket *udp;
- (void)viewDidLoad
{
    [super viewDidLoad];
    tcp=[[DCTCPSocket alloc]initWithAddressAndPort:@"127.0.0.1" :@"9000"];
    udp=[[DCUDPSocket alloc]initWithAddressAndPort:@"127.0.0.1" :@"9000"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)write:(id)sender {
    [udp sendMessage:@"hello"];
    // [tcp sendMessage:_message.text];
}

- (IBAction)read:(id)sender {
    NSLog(@"%@",[udp Read]);
    // NSLog(@"%@",[tcp Reading]);
}
@end
