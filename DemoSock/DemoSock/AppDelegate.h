//
//  AppDelegate.h
//  DemoSock
//
//  Created by Flexkid on 21/10/13.
//  Copyright (c) 2013 Diego Caridei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
