# DCConnect Framework 1.0
DCConnect allows you to send and receive information via TCP and UDP.

![create-custom-ios-framework.png](https://bitbucket.org/repo/KG9qMp/images/838358306-create-custom-ios-framework.png)

#How to use
1)import DCConnect.framework into your project
![screenshot](http://diegocaridei.altervista.org/blog/wp-content/uploads/2013/10/import.png)
2)import the framework into the class
#import <DCConnect/DCConnect.h>


## TCP Example
- (void)open;
- (void)close;
-(void)sendMessage:(NSString*)data;
-(void)connectionforIpAddress:(NSString *)ipAddress forPort:(NSString *)portNo;
-(NSString*)Reading;
-(DCTCPSocket*)initWithAddressAndPort:(NSString*)address :(NSString*)port;


#Init With Address and Port
 
DCTCPSocket* tcp=[[DCTCPSocket alloc]initWithAddressAndPort:@"127.0.0.1" :@"9000"];

#Send the message

[tcp sendMessage:@"Hello"];

#Read the message

NSLog(@"%@",[tcp Reading]);

## UDP Example

-(void)Connection:(NSString*)hostname :(NSString*)PortNumber;
-(void)sendMessage:(NSString*)Message;
-(NSMutableString*)Read;
-(void)Reading;
-(DCUDPSocket*)initWithAddressAndPort:(NSString*)address :(NSString*)port;

#Init With Address and Port

DCUDPSocket  *udp=[[DCUDPSocket alloc]initWithAddressAndPort:@"127.0.0.1" :@"9000"];

#Send the message

[udp sendMessage:@"hello"];

#Read the message

NSLog(@"%@",[udp Read]);
